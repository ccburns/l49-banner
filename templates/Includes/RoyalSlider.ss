<% if $getRoyalSliderItems %>
<div class="header-sliders">
    <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1600px; height: 0; overflow: hidden; visibility: hidden;">
        <% if $getRoyalSliderItems.Count > 1 %>
            <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
                <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                <div style="position:absolute;display:block;background:url('/L49-banner/img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
            </div>
        <% end_if %>
        <% loop $getRoyalSliderItems %>
            <!-- Using the first image in the slider to set the height -->
            <% if $Pos == 1 %>
            <div data-u="slides" data-test="colin" style="cursor: default; position: relative; top: 0px; left: 0px; width: {$SliderImage.Width}px; height: {$SliderImage.Height}px; overflow: hidden;">
            <% end_if %>
        <% end_loop %>
            <% loop $getRoyalSliderItems %>
                <div data-p="225.00">
                    <% if $InternalExternal == 'NoLink' %>
                        <img src="{$SliderImage.Url}" class="banner-image-{$Pos}" />
                    <% else %>
                        <a href="<% if $InternalExternal == 'External' %>{$ExternalLink}<% else %>{$InternalLinkUrl}<% end_if %>"<% if $OpenNewWindow == 1 %> target="_blank"<% end_if %>>
                            <img src="{$SliderImage.Url}" class="banner-image-{$Pos}" />
                        </a>
                    <% end_if %>
                </div>
            <% end_loop %>
        </div>
        <% if $getRoyalSliderItems.Count > 1 %>
            <div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
                <div data-u="prototype" style="width:16px;height:16px;"></div>
            </div>
            <span data-u="arrowleft" class="jssora22l" style="top:0px;left:8px;width:40px;height:58px;" data-autocenter="2"></span>
            <span data-u="arrowright" class="jssora22r" style="top:0px;right:8px;width:40px;height:58px;" data-autocenter="2"></span>
        <% end_if %>
    </div>
</div>
<% end_if %>