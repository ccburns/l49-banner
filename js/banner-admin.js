jQuery(document).ready(function($) {

    if($("#Form_ItemEditForm_InternalExternal_Internal:checked").length > 0){
        $('#Form_ItemEditForm_InternalLink_Holder').show();
    }else{
        $('#Form_ItemEditForm_InternalLink_Holder').hide();
    }
    if($("#Form_ItemEditForm_InternalExternal_External:checked").length > 0){
        $('#Form_ItemEditForm_ExternalLink_Holder').show();
    }else{
        $('#Form_ItemEditForm_ExternalLink_Holder').hide();
    }

    $(document).on("change", "#Form_ItemEditForm_InternalExternal_NoLink", function(e){
        e.preventDefault();
        $('#Form_ItemEditForm_InternalLink_Holder').hide();
        $('#Form_ItemEditForm_ExternalLink_Holder').hide();
        $('#Form_ItemEditForm_OpenNewWindow_Holder').hide();
    });

    $(document).on("change", "#Form_ItemEditForm_InternalExternal_Internal", function(e){
        e.preventDefault();
        $('#Form_ItemEditForm_InternalLink_Holder').hide();
        $('#Form_ItemEditForm_ExternalLink_Holder').hide();

        $('#Form_ItemEditForm_InternalLink_Holder').show();
        $('#Form_ItemEditForm_InternalLink_Holder').val(true);
        $('#Form_ItemEditForm_OpenNewWindow_Holder').show();
    });

    $(document).on("change", "#Form_ItemEditForm_InternalExternal_External", function(e){
        e.preventDefault();
        $('#Form_ItemEditForm_InternalLink_Holder').hide();
        $('#Form_ItemEditForm_ExternalLink_Holder').hide();

        $('#Form_ItemEditForm_ExternalLink_Holder').show();
        $('#Form_ItemEditForm_ExternalLink_Holder').val(true);
        $('#Form_ItemEditForm_OpenNewWindow_Holder').show();
    });

    // var ParentPageID = $('#Form_ItemEditForm_CMSMainCurrentPageID').val();
    // $('#Form_ItemEditForm_PageID').val(ParentPageID);
    // alert(ParentPageID);

});